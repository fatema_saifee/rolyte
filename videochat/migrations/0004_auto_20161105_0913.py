# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videochat', '0003_auto_20161102_0330'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chat',
            name='contact',
            field=models.ForeignKey(related_name='chat', to='opal.UserProfile', null=True),
        ),
    ]
