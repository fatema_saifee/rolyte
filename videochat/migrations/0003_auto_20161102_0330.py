# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videochat', '0002_auto_20150610_2114'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chat',
            name='chat_status',
            field=models.CharField(default=b'Initialize', max_length=10, choices=[(b'Initialize', b'Initializing'), (b'Active', b'Active'), (b'Waiting', b'Waiting'), (b'Terminated', b'Terminated')]),
        ),
        migrations.AlterField(
            model_name='chat',
            name='chatname',
            field=models.CharField(max_length=100),
        ),
    ]
