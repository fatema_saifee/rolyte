"""
inayo - Our OPAL Application
"""
from opal.core import application

class Application(application.OpalApplication):
    schema_module = 'inayo.schema'
    flow_module   = 'inayo.flow'
    javascripts   = [
        'js/inayo/routes.js',
        'js/opal/controllers/discharge.js',
        # Uncomment this if you want to implement custom dynamic flows.
        # 'js/inayo/flow.js',
    ]