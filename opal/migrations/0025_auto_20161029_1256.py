# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import opal.models


class Migration(migrations.Migration):

    dependencies = [
        ('opal', '0024_auto_20161029_1210'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='image',
            field=models.ImageField(default=None, null=True, upload_to=opal.models.generate_filename, blank=True),
        ),
        migrations.RemoveField(
            model_name='userprofile',
            name='roles',
        ),
        migrations.AddField(
            model_name='userprofile',
            name='roles',
            field=models.ForeignKey(default=None, blank=True, to='opal.Role', null=True),
        ),
    ]
