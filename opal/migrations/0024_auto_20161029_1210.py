# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('opal', '0023_auto_20160630_1340'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='address_line1',
            field=models.CharField(max_length=45, null=True, verbose_name=b'Address line 1', blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='address_line2',
            field=models.CharField(max_length=45, null=True, verbose_name=b'Address line 2', blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='city',
            field=models.CharField(max_length=50, blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='county',
            field=models.CharField(max_length=40, null=True, verbose_name=b'County', blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='email',
            field=models.EmailField(default=None, max_length=70, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='phone',
            field=models.CharField(default=None, max_length=13, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='post_code',
            field=models.CharField(max_length=10, null=True, verbose_name=b'Post Code', blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='speciality',
            field=models.ManyToManyField(default=None, to='opal.Speciality', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='roles',
            field=models.ManyToManyField(default=None, to='opal.Role', null=True, blank=True),
        ),
    ]
