# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('opal', '0028_auto_20161029_1404'),
    ]

    operations = [
        migrations.AddField(
            model_name='speciality',
            name='description',
            field=models.CharField(max_length=1000, null=True, blank=True),
        ),
    ]
