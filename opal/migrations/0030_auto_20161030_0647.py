# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('opal', '0029_speciality_description'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='speciality',
            options={'verbose_name_plural': 'Specialities'},
        ),
        migrations.AlterField(
            model_name='speciality',
            name='description',
            field=models.TextField(max_length=1000, null=True, blank=True),
        ),
    ]
